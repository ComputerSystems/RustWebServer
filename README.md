# High Performance Web Server in Rust

### Abstract:
We are developing a web server that serves static files in a relatively new programming language - Rust. This web server uses event-driven architecture by implementing Reactor Pattern. There is a main thread that will accept requests from the clients and it will send these requests to multiple worker threads(Reactors) each running on a separate core of the machine. These worker threads will periodically iterate over the request queue and read a chunk of file and write it to the stream. We use in-memory cache to improve the performance. We have evaluated this server using Apache Benchmark tool and it provides reasonable performance compared to Nginx and Apache web servers.  
Contribution
Both the project members have contributed equally to Overall system design, Request processing and Evaluation works. Following are the individual contributions:
Reactor model implementation – Bishwajeet Dey
Caching implementation – Sriramprabhu Sankaraguru
### 1. Introduction
Our motivation towards this project is that, we wanted to learn about the web server architecture and use the features of the Rust programming language. Rust is a safe, concurrent and practical systems language. Its syntax is similar to C++ but offers various features like Memory and Concurrency safety and Better Type system. Rust’s RAII feature will make sure that the unused resources are cleaned up once the variable goes out of scope. This will help developers to program without worrying about memory leaks and segmentation faults. If, a developer forgot to close a socket FD or free the buffer, it may lead to memory leaks in other programming languages like C. But Rust will make sure that these resources are cleaned up automatically once these variables are not used anymore. In a long run, where we add various features to the web server and try to establish a production grade web server, this will help us a lot in maintenance. Memory leaks and poor concurrency may lead to system reboot resulting in undesired down time.
### 2. Novelty
Reactor pattern to handle web requests. 
Using Rust to achieve performance and Memory, Type safety to achieve a stable and easily maintainable system.
### 3. Design

   ![Reactor Pattern](./ppt/reactor.png "Reactor Pattern")


   ![Design](./ppt/design.png "Overall System Design")

This is the web server model that we are using. There is a main thread that runs on one core of the machine which accepts requests from the clients and adds it to the pending queue of the workers. There will be one worker running in each of the remaining cores of the machines. Each worker will have its own cache associated with it to store the small objects in memory. Each worker thread will periodically switch between processing the pending requests and accepting new requests. 
Thread based Approach vs Event-driven Approach
There are 2 approaches to handle the web request. One is thread based approach. In which we spawn a thread for each incoming connection. This doesn’t scale well as the number of concurrent users increase. In event based approach, we separate threads from connections. Reactor pattern is one implementation of the event-driven approach. We will see that in detail.
###### 3.a. Reactor Pattern
 
Here, an event loop is running which accepts the connections from the clients. This thread will just delegate the request to respective handler and start accepting other requests. So, it doesn’t block on long running task like I/O. In our design, each worker is a reactor. We run the reactor in each core of the system. It accepts the TCP Stream from the main thread and process then sequentially by reading chunk of file and writing it to the stream. Once the job is done, we remove the request from the queue.

###### 3.b. Cache Manager 

To improve the response time of the query, we have a cache manager associated with each worker thread. It stores the contents of the frequently queried files. We have a limit on the maximum size of the file that can be stored in the memory. When a file request comes, we check the cache first and if it is present in the memory, we serve it from there. It reduces the disk I/O operations and improves the response time considerably. This cache uses LRU replacement policy. Cache manager also maintains freshness of the contents. When the content is modified, it will fetch the new contents instead of serving the old content. We have also provided optional compression feature. This will compress the contents of the files and store it in the cache. So, we can have more files in memory.

###### 3.c. Rate limiting

The main thread can drop the request/send error message if the worker queue is full. Thus, we can have some protection against the Denial of Service attacks.

### 4. Implementation

###### 4.a. Reactor

The main thread is just going to move the TCP Stream from the client connection to the worker. Worker is going to read the stream and get the HTTP request from the user and add it to the request queue. It then periodically iterates the queue and reads small chunk of data and writes it to appropriate stream. Once this iteration is over, it starts accepting requests. Once the file is completely written to the stream, it removes the request from the queue. It also contacts the cache before start reading from the disk. When a request comes, worker reads the metadata of the file and get the size and last modified time. If the size is less than the maximum file size a cache holds(50KB), then it queries the cache manager for the file contents.

###### 4.b. Cache Manager

Each worker thread will maintain a cache manager that can hold fixed number of small objects (<50KB). Cache stores the mapping between filename and its contents + metadata (last modified time). Cache uses LRU replacement policy. The cache manager will be queried with the filename, last modified time from the worker. It will first check if the entry is present in the cache. If the entry is present, it will server from memory. Cache manager maintains the freshness of the cached file by comparing the metadata stored along with it. It always servers fresh content to the client. For compression, we use Bzip compression technique. This reduces the size of the contents and enables us to store more data in memory. But when the cache hit rate is poor/there are fewer files where all of them can fit into cache, compression doesn’t yield the best result as we end up doing extra work.

###### 4.c. Rate limiting
Main thread will check the size of the pending queue of the worker thread before adding the request. If the queue is already full, it will send error message (503) to the client saying the server can’t accept any more request.
Technical Challenges
We first started with Thread per connection model which didn’t yield expected performance. Then we learned about the Nginx server model and moved to Event-driven architecture.
Being new to the Rust’s ownership model, we find it difficult to move the variables across functions. It took us time to read about the Rust type system and program accordingly.
When the caching is added, we found that it had no effect on performance. As we revisited the code, we found that we clone the file content stored in the cache every time we return it as Rust doesn’t allow multiple ownerships. As the object size is big (50KB), calling Clone affected the performance badly. We fixed it and that improved the performance greatly.
### 5. Evaluation
Performance of this web server was evaluated using Apache Benchmark tool. The results were compared with Nginx and Apache web servers. 
###### 5.a. Testing Environment & Configuration
We have used AWS EC2 instances to test our server. Following are the configuration of the machines.

Conf | Server |	Client
--- | --- | ---
Memory	| 8 GB	| 4 GB
CPU Speed |	3.0 GHz	| 3.0 GHz
Cores | 4 | 2
Disk Type |  SSD | SSD

###### 5.b. Results
Web server was hosted in the server machine and the test was executed from the client machine. Test was conducted on different formats of the files (JPG, GIF, HTML, PNG, CSS etc.) Various requests and concurrency parameters were given to test the performance during Heavy load, Medium load and light load scenarios.
Following are the test results:
###### 5.b.1 Light Load
Following are the results when the Number of requests are set to 1000 and concurrency is set to 10 and 100. 
   
###### 5.b.2 Medium Load
Following are the results for medium load with requests set to 10000 and concurrency set to 100 and 1000.
   
###### 5.b.3 Heavy Load
Following are the results when requests are set to 100,000 and concurrency is set to 1000 and 5000.

![Result](./ppt/result.png "Results")

### 6. Conclusion
From the evaluation, we can observe that our web server performance is still doesn’t match the performances of Apache and Nginx web servers. One reason could be that our server is single process model. But apache and Nginx are multi-process model. 
One more interesting fact is that as the load increases, Apache and Nginx started sending 503 error message but our server can still handle those connections.
###### 6.a Future Work
We can try to add few of the following features to improve the performance of the web server further.
Rate limiting can be done to block the request from attackers instead of blocking the legitimate connections. We need to check the IP Address of the client and block requests from client if they make more than k request in n milliseconds. 
We can make this as multi-process model to improve the performance.

